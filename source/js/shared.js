function sharedLogic() {

   // Equal Height Method
   const equalHeight = (className) => {

      var findClass = document.getElementsByClassName(className);
      var tallest = 0;
      var i = 0;
      // Loop over matching divs
      for (i = 0; i < findClass.length; i++) {
         var ele = findClass[i];
         ele.style.height = "auto";
         var eleHeight = ele.offsetHeight;
         tallest = (eleHeight > tallest ? eleHeight : tallest); /* look up ternary operator if you dont know what this is */
      }
      for (i = 0; i < findClass.length; i++) {
         findClass[i].style.height = tallest + "px";
      }
      // console.log(tallest);
   }
   new equalHeight("sameHeight");

   // Re run if window resize
   window.addEventListener('resize', () => {
      new equalHeight("sameHeight");
   });

   // Back to top button position 
   const backToTop = () => {
      const socialFloat = document.querySelector('#backToTopButton');
      const footer = document.querySelector('#footer');

      const checkOffset = () => {

         function getRectTop(el) {
            return el.getBoundingClientRect().top;
         }

         if ((getRectTop(socialFloat) + document.body.scrollTop) + socialFloat.offsetHeight > (getRectTop(footer) + document.body.scrollTop))
            socialFloat.style.position = 'absolute';
            if(window.scrollY > 499) {
               socialFloat.classList.add('active');
            }else {
               socialFloat.classList.remove('active');
            }
         if (document.body.scrollTop + window.innerHeight < (getRectTop(footer) + document.body.scrollTop))
            socialFloat.style.position = 'fixed'; // restore when you scroll up

         // socialFloat.innerHTML = document.body.scrollTop + window.innerHeight;
      }

      document.addEventListener('scroll', () => {

         setTimeout(() => {
            checkOffset();
         }, 100);
         
      });

   }
   backToTop();



// Smooth Scroll to bottom
function smoothScroll(target, duration) {
   var currentTarget = document.querySelector(target);
   var positionTarget = currentTarget.getBoundingClientRect().top;
   var positionStart = window.pageYOffset;
   var objectDistance = positionTarget - positionStart;
   let startTime = null;



   function animationTime(currentTime) {
      if (startTime === null) startTime = currentTime;
      var elapsedTime = currentTime - startTime;
      var animationRun = animationEase(elapsedTime, positionStart, objectDistance, duration);
      window.scrollTo(0, animationRun);
      if (elapsedTime < duration) requestAnimationFrame(animationTime);
   }

   function animationEase(t, b, c, d) {
      t /= d / 2;
      if (t < 1) return c / 2 * t * t * t + b;
      t -= 2;
      return c / 2 * (t * t * t + 2) + b;
   }

   requestAnimationFrame(animationTime);
};

const scrollClick = document.querySelectorAll('.about-scroll-down');
for (var i = 0; i < scrollClick.length; i++) {
   scrollClick[i].addEventListener('click', () => {
      smoothScroll('#visionMission', 1750);
   });
}


const clickToTop = document.getElementById('backToTop');
clickToTop.addEventListener('click', () => {
   console.log('Scroll!');
   smoothScroll('#top', 1750);
})





}

export default sharedLogic;