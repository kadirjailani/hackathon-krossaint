function topNavi() {

    // Scroll then change top navi style
    window.addEventListener("scroll", () => {

        setTimeout(() => {



            let navBar = document.getElementById('topNavi'); // Top navi
            const navBarActive = navBar.classList; // Top navi classes
            const scrollPosition = window.scrollY; // Y position
            const scrollReachBottom = document.documentElement.scrollHeight - window.innerHeight; // Y Bottom position

            // console.log(scrollPosition, scrollReachBottom);
            if (scrollPosition >= 200) {
                navBarActive.add('scrolled');
            } else {
                navBarActive.remove('scrolled');
            }
        }, 1000);
    });



    const mobileMenu = () => {
        let toggleButton = document.getElementsByClassName('toggleMobileMenu');
        let menuListClasses = document.getElementById('menuList').classList;

        for (var i = 0; i < toggleButton.length; i++) {

            toggleButton[i].addEventListener("click", () => {
                if (menuListClasses.contains('active-mobile')) {
                    menuListClasses.remove('active-mobile');
                } else {
                    menuListClasses.add('active-mobile');
                }
            })
        }

    }
    mobileMenu();

}

export default topNavi;