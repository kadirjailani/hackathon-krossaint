// Import Styling //
import 'bootstrap';
// import '@fortawesome/fontawesome-free/js/all';
import './scss/main.scss';

// Import Function //
import Vue from 'vue';
import VueResource from 'vue-resource';
// import VeeValidate from 'vee-validate';
import Vuelidate from 'vuelidate';

// Import Componenet
// import "./js/main.js";
import SearchPage from "./searchPage.vue";


// Vue Component //
Vue.use(VueResource);
// Vue.use(VeeValidate);
// Vue.use(Vuelidate);
// formWarta();

new Vue({
   el: '#krossaint',
   render: h => h(SearchPage)
});